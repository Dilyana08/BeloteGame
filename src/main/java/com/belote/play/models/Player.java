package com.belote.play.models;

import java.util.ArrayList;
import java.util.List;

public final class Player {
	String name;
	List<Card> hand;
	Team team;
	boolean human;

	Player(String name, boolean human) {
		this.name = name;
		this.human = human;
		hand = new ArrayList<Card>();
	}
	
	public void addCardToHand(Card card){
		hand.add(card);
	}
	
	public void removeCardFromHand(Card card)
	{
		hand.remove(card);
	}

	public String getName() {
		return name;
	}

	public List<Card> getHand() {
		return hand;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public boolean isHuman() {
		return human;
	}

}

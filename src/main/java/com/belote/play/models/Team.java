package com.belote.play.models;

import java.util.List;

public final class Team {
	String teamName;
	List<Player> players;
	int pointsForRound;
	int pointsForGame;
	
	public Team(String name, List<Player> players){
		this.teamName = name;
		this.players = players;
		pointsForGame = 0;
		pointsForRound = 0;
	}
	
	public String getTeamName() {
		return teamName;
	}
	
	public List<Player> getPlayers() {
		return players;
	}
	
	public int getPointsForRound() {
		return pointsForRound;
	}
	
	public void setPointsForRound(int pointsForRound) {
		this.pointsForRound = pointsForRound;
	}
	
	public int getPointsForGame() {
		return pointsForGame;
	}
	
	@Override
	public String toString() {
		return "Team [teamName=" + teamName + ", players=" + players + ", pointsForRound=" + pointsForRound
				+ ", pointsForGame=" + pointsForGame + "]";
	}

}

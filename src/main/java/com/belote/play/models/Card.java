package com.belote.play.models;

import com.belote.play.enums.Bids;
import com.belote.play.enums.Colours;
import com.belote.play.enums.Ranks;

public final class Card {
	Colours colour;
	Ranks rank;
	
	public Card(Ranks rank, Colours colour){
		this.colour = colour;
		this.rank = rank;
	}
	
	public Colours getColour() {
		return colour;
	}
	
	public Ranks getRank() {
		return rank;
	}
	
	public int score(Bids trump){
		return 0;
		//TODO scores
	}
	
	public boolean match(Colours colourOfCard){
		return (getColour() == colourOfCard);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((colour == null) ? 0 : colour.hashCode());
		result = prime * result + ((rank == null) ? 0 : rank.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Card other = (Card) obj;
		if (colour != other.colour)
			return false;
		if (rank != other.rank)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Card [colour=" + colour + ", rank=" + rank + "]";
	}

}

 package com.belote.play.decks;

import java.util.List;
import java.util.Stack;

import com.belote.play.models.Card;
import com.belote.play.models.Player;

public class BeloteDeck extends Deck {

	public BeloteDeck(Stack<Card> cards) {
		super(cards);
	}

	
	@Override
	public void firstDeal(List<Player> players) {
		for (int i = 0; i < 3; i++) {
			dealOneCardPerPlayer(players);
		}
		
		for (int i = 0; i < 2; i++) {
			dealOneCardPerPlayer(players);
		}
		
		//TODO: add sort
		
	}

	@Override
	public void extraDeal(List<Player> players) {
		for (int i = 0; i < 3; i++) {
			dealOneCardPerPlayer(players);
		}
		
		//TO DO: add sort
		
	}
	
	

}

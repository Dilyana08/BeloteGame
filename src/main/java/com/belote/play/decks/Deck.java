package com.belote.play.decks;

import java.util.List;
import java.util.Stack;

import com.belote.play.models.Card;
import com.belote.play.models.Player;

public abstract class Deck {
	final Stack<Card> cards;
	
	protected Deck(Stack<Card> cards){
		this.cards = cards;
	}
	
	public abstract void firstDeal(List<Player> players);
	public abstract void extraDeal(List<Player> players);
	
	protected void dealOneCardPerPlayer(List<Player> players){
		for (Player player : players) {
			player.addCardToHand(cards.pop());
		}
	}
	
	public boolean isEmpty(){
		return cards.isEmpty();
	}

}

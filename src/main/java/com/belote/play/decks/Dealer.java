package com.belote.play.decks;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

import com.belote.play.enums.Colours;
import com.belote.play.enums.Ranks;
import com.belote.play.models.Card;

public class Dealer {

	private Dealer(){};
	
	private static Stack<Card> createDeck(List<Card> cards) {
		Dealer.shuffle(cards);
		Dealer.split(cards);
		Stack<Card> stackOfCards = Dealer.toStack(cards);
		return stackOfCards;
	}

	private static void split(List<Card> cards) {
		for (int i = 0; i < cards.size() / 2; i++) {
			Collections.swap(cards, i, cards.size() - 1 - i);
		}
	}

	private static void shuffle(List<Card> cards) {
		Collections.shuffle(cards);
	}

	private static Stack<Card> toStack(List<Card> cards) {
		Stack<Card> stack = new Stack<Card>();
		for (Card card : cards) {
			stack.push(card);
		}
		return stack;
	}
	
	public static Deck beloteDeck(){
		List<Card> cards = new ArrayList<Card>();
		
		for (Ranks rank : Ranks.values()) {
			for (Colours colour : Colours.values()) {
				cards.add(new Card(rank, colour));
			}
		}
		
		Stack<Card> stack = createDeck(cards);
		return new BeloteDeck(stack);
	}
}

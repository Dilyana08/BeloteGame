package com.belote.play.game;

import java.util.List;
import java.util.Map;

import com.belote.play.interfaces.Game;
import com.belote.play.models.Team;

public abstract class CountScoreForGame implements Game{
	private List<Team> teams;
	private Map<Team, Integer> score;
	
	protected CountScoreForGame(List<Team> teams){
		this.teams = teams;
	}
	
	public abstract int countScore(Team team);
	
	public String displayScore(Team team){
		return null;
		//TODO make function
	}

	public abstract void play();
	

}

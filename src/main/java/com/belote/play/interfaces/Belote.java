package com.belote.play.interfaces;

import java.util.List;
import java.util.Map;

import com.belote.play.enums.Bids;
import com.belote.play.models.Player;
import com.belote.play.models.Team;

public interface Belote {
	abstract Bids bid(List<Player> players);
	abstract void play(Bids bid, List<Player> players);
	abstract Map<Team,Integer> countScore(List<Player> players);
	

}
